from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from rest_framework import viewsets, permissions, generics

from invoice.forms import InvoiceForm
from invoice.models import Invoice
from invoice.serializer import InvoiceSerializer


class InvoiceViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer
    permission_classes = [permissions.IsAdminUser]





class ListInvoiceView(ListView):
    model = Invoice
    queryset = Invoice.objects.all()
    template_name = 'invoice/invoice_list.html'
    context_object_name = 'invoice_list'


class RegisterInvoiceView(CreateView):
    form_class = InvoiceForm
    model = Invoice
    template_name = 'invoice/add_order.html'
    success_url = reverse_lazy('invoice:invoice_list')


class UpdateInvoiceView(UpdateView):
    model = Invoice
    form_class = InvoiceForm
    pk_url_kwarg = 'pk'
    template_name = 'invoice/add_order.html'
    success_url = reverse_lazy('invoice:invoice_list')


class DeleteInvoiceView(DeleteView):
    model = Invoice
    pk_url_kwarg = 'pk'
    template_name = 'invoice/delete_invoice.html'
    success_url = reverse_lazy('invoice:invoice_list')
