from django.db import models
from django.contrib.auth.models import User
from persons.models import Client, Waiter
from tables.models import Table


class Invoice(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    waiter = models.ForeignKey(Waiter, on_delete=models.CASCADE)
    table = models.ForeignKey(Table, on_delete=models.CASCADE)

    def __str__(self):
        return f'Invoice #{self.id}'
