from django.urls import path
from rest_framework import routers

from . import views
from .views import ListInvoiceView, RegisterInvoiceView, UpdateInvoiceView, DeleteInvoiceView

app_name = 'invoice'

urlpatterns = [
    path('invoice_list', ListInvoiceView.as_view(), name='invoice_list'),
    path('add_invoice', RegisterInvoiceView.as_view(), name='add_invoice'),
    path('update_invoice/<int:pk>/', UpdateInvoiceView.as_view(), name='update_invoice'),
    path('delete_invoice/<int:pk>/', DeleteInvoiceView.as_view(), name='delete_invoice'),
    ]

router = routers.DefaultRouter()
router.register(r'invoices', views.InvoiceViewSet)