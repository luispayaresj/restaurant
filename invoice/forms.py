from django import forms
from django.forms import TextInput

from invoice.models import Invoice


class InvoiceForm(forms.ModelForm):
    class Meta:
        model = Invoice
        fields = '__all__'

        widgets = {
            'reference': TextInput(attrs={
                'class': "form-control mb-3",
                'style': 'max-width: 500px;',
            }),
        }