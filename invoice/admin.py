from django.contrib import admin

from invoice.models import Invoice


@admin.register(Invoice)
class InvoiceAdmin(admin.ModelAdmin):
    list_display = ('id', 'client', 'waiter', 'table')
    search_fields = ('client', 'waiter', )
    list_filter = ('waiter', )
