
## dependecies
* python 3.9
* Django 4.0.4
* sqlite
* django-crispy-forms 1.14.0
* Django Rest Framework 3.13.1 
* djangorestframework-simplejwt 5.1.0




---



* Note: The db.sqlite3 database is uploaded because there are elements there without which the manual tests of the interface would not work
---

## Apps
### 1. Persons
#### Models: Client,Waiter
### 2. Products
#### Models: TypeProduct, Product, Order 
### 3. Invoice 
#### Models: Invoice
### 4. Tables
#### Models: Table


---
## Entity-Relation Model

![](restaurant.drawio.png)



