from django.db import models

from invoice.models import Invoice


class TypeProduct(models.Model):
    name = models.CharField(max_length=150)

    def __str__(self):
        return f'{self.name}'


class Product(models.Model):
    name = models.CharField(max_length=50)
    importe = models.IntegerField()
    type = models.ForeignKey(TypeProduct, on_delete=models.CASCADE)
    price = models.FloatField()

    def __str__(self):
        return f'{self.name}, {self.type}'


class Order(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    invoice = models.ForeignKey(Invoice, on_delete=models.CASCADE)
    quantity = models.IntegerField()

    def __str__(self):
        return f'{self.product.name}, {self.quantity}'
