from django import forms
from django.forms import TextInput

from products.models import Product, TypeProduct, Order


class TypeProductForm(forms.ModelForm):
    class Meta:
        model = TypeProduct
        fields = '__all__'

        widgets = {
            'reference': TextInput(attrs={
                'class': "form-control mb-3",
                'style': 'max-width: 500px;',
            }),
        }


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = '__all__'

        widgets = {
            'reference': TextInput(attrs={
                'class': "form-control mb-3",
                'style': 'max-width: 500px;',
            }),
        }


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = '__all__'

        widgets = {
            'reference': TextInput(attrs={
                'class': "form-control mb-3",
                'style': 'max-width: 500px;',
            }),
        }
