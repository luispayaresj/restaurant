from products.models import Order, Product, TypeProduct
from rest_framework import serializers


class TypeProductSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TypeProduct
        fields = '__all__'


class ProductSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'


class OrderSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'




