from django.urls import path
from rest_framework import routers

from . import views
from .views import ListTypeProductView, RegisterTypeProductView, UpdateTypeProductView, \
    DeleteTypeProductView, ListProductView, UpdateProductView, DeleteProductView, RegisterProductView, ListOrderView, \
    UpdateOrderView, RegisterOrderView, DeleteOrderView

app_name = 'products'

urlpatterns = [
    path('typeProduct_list', ListTypeProductView.as_view(), name='typeProduct_list'),
    path('add_typeProduct', RegisterTypeProductView.as_view(), name='add_typeProduct'),
    path('update_typeProduct/<int:pk>/', UpdateTypeProductView.as_view(), name='update_typeProduct'),
    path('delete_typeProduct/<int:pk>/', DeleteTypeProductView.as_view(), name='delete_typeProduct'),

    path('product_list', ListProductView.as_view(), name='product_list'),
    path('add_product', RegisterProductView.as_view(), name='add_product'),
    path('update_product/<int:pk>/', UpdateProductView.as_view(), name='update_product'),
    path('delete_product/<int:pk>/', DeleteProductView.as_view(), name='delete_product'),

    path('order_list', ListOrderView.as_view(), name='order_list'),
    path('add_order', RegisterOrderView.as_view(), name='add_order'),
    path('update_order/<int:pk>/', UpdateOrderView.as_view(), name='update_order'),
    path('delete_order/<int:pk>/', DeleteOrderView.as_view(), name='delete_order'),
    ]

router = routers.DefaultRouter()
router.register(r'product', views.ProductViewSet)
router.register(r'order', views.OrderViewSet)
router.register(r'type_product', views.TypeProductViewSet)