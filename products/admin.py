from django.contrib import admin

from products.models import Product, Order, TypeProduct


@admin.register(Product)
class DishAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'importe', 'type', 'price')
    search_fields = ('name', 'type', 'importe')
    list_filter = ('name', 'type', 'importe', 'price')


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'product', 'quantity', 'invoice')
    search_fields = ('product',)
    list_filter = ('product',)

@admin.register(TypeProduct)
class TypeProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ('name',)
    list_filter = ('name',)
