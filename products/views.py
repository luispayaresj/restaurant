from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from rest_framework import viewsets, permissions

from products.forms import OrderForm, ProductForm, TypeProductForm
from products.models import Order, Product, TypeProduct
from products.serializer import ProductSerializer, OrderSerializer, TypeProductSerializer


class TypeProductViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = TypeProduct.objects.all()
    serializer_class = TypeProductSerializer
    permission_classes = [permissions.IsAdminUser]


class ProductViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [permissions.IsAdminUser]


class OrderViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = [permissions.IsAuthenticated]


class ListTypeProductView(ListView):
    model = TypeProduct
    queryset = TypeProduct.objects.all()
    template_name = 'typeProduct/typeProduct_list.html'
    context_object_name = 'typeProduct_list'


class RegisterTypeProductView(CreateView):
    form_class = TypeProductForm
    model = TypeProduct
    template_name = 'typeProduct/add_typeProduct.html'
    success_url = reverse_lazy('products:typeProduct_list')


class UpdateTypeProductView(UpdateView):
    model = TypeProduct
    form_class = TypeProductForm
    pk_url_kwarg = 'pk'
    template_name = 'typeProduct/add_typeProduct.html'
    success_url = reverse_lazy('products:typeProduct_list')


class DeleteTypeProductView(DeleteView):
    model = TypeProduct
    pk_url_kwarg = 'pk'
    template_name = 'typeProduct/delete_typeProduct.html'
    success_url = reverse_lazy('products:typeProduct_list')


class ListProductView(ListView):
    model = Product
    queryset = Product.objects.all()
    template_name = 'product/product_list.html'
    context_object_name = 'product_list'


class RegisterProductView(CreateView):
    form_class = ProductForm
    model = Product
    template_name = 'product/add_product.html'
    success_url = reverse_lazy('products:product_list')


class UpdateProductView(UpdateView):
    model = Product
    form_class = ProductForm
    pk_url_kwarg = 'pk'
    template_name = 'product/add_product.html'
    success_url = reverse_lazy('products:product_list')


class DeleteProductView(DeleteView):
    model = Product
    pk_url_kwarg = 'pk'
    template_name = 'product/delete_product.html'
    success_url = reverse_lazy('products:product_list')


class ListOrderView(ListView):
    model = Order
    queryset = Order.objects.all()
    template_name = 'order/order_list.html'
    context_object_name = 'order_list'


class RegisterOrderView(CreateView):
    form_class = OrderForm
    model = Order
    template_name = 'order/add_order.html'
    success_url = reverse_lazy('products:order_list')


class UpdateOrderView(UpdateView):
    model = Order
    form_class = OrderForm
    pk_url_kwarg = 'pk'
    template_name = 'order/add_order.html'
    success_url = reverse_lazy('products:order_list')


class DeleteOrderView(DeleteView):
    model = Order
    pk_url_kwarg = 'pk'
    template_name = 'order/delete_order.html'
    success_url = reverse_lazy('products:order_list')
