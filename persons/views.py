# from django.contrib.auth import logout, authenticate, login
# from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.views.generic import ListView, TemplateView, CreateView, UpdateView, DeleteView, FormView, RedirectView
# from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from rest_framework import viewsets, permissions

from persons.forms import ClientForm, WaiterForm
from persons.models import Client, Waiter
from persons.serializer import ClientSerializer, WaiterSerializer


class ClientViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    permission_classes = [permissions.IsAdminUser]


class WaiterViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Waiter.objects.all()
    serializer_class = WaiterSerializer
    permission_classes = [permissions.IsAdminUser]


class HomeView(TemplateView):
    """
        This view shows the home page of the app.
    """
    template_name = "base.html"


class ListClientView(ListView):
    model = Client
    queryset = Client.objects.all()
    template_name = 'persons/client_list.html'
    context_object_name = 'client_list'


class ListWaiterView(ListView):
    model = Waiter
    queryset = Waiter.objects.all()
    template_name = 'persons/waiter_list.html'
    context_object_name = 'waiter_list'


class RegisterClientView(CreateView):
    form_class = ClientForm
    model = Client
    template_name = 'persons/add_client.html'
    success_url = reverse_lazy('persons:client_list')


class RegisterWaiterView(CreateView):
    form_class = WaiterForm
    model = Waiter
    template_name = 'persons/add_waiter.html'
    success_url = reverse_lazy('persons:waiter_list')


class UpdateClientView(UpdateView):
    model = Client
    form_class = ClientForm
    pk_url_kwarg = 'pk'
    template_name = 'persons/add_client.html'
    success_url = reverse_lazy('persons:client_list')


class UpdateWaiterView(UpdateView):
    model = Waiter
    form_class = WaiterForm
    pk_url_kwarg = 'pk'
    template_name = 'persons/add_waiter.html'
    success_url = reverse_lazy('persons:waiter_list')


class ClientDeleteView(DeleteView):
    model = Client
    pk_url_kwarg = 'pk'
    template_name = 'persons/delete_client.html'
    success_url = reverse_lazy('persons:client_list')


class WaiterDeleteView(DeleteView):
    model = Waiter
    pk_url_kwarg = 'pk'
    template_name = 'persons/delete_waiter.html'
    success_url = reverse_lazy('persons:waiter_list')


# class LoginView(FormView):
#     form_class = AuthenticationForm
#     template_name = 'registration/login.html'
#     success_url = reverse_lazy('persons:home')
#
#     def form_valid(self, form):
#         username = form.cleaned_data['username']
#         password = form.cleaned_data['password']
#         user = authenticate(username=username, password=password)
#
#         if user is not None and user.is_active:
#             login(self.request, user)
#             return super(LoginView, self).form_valid(form)
#         else:
#             return self.form_invalid(form)
#
#
# class RegisterView(CreateView):
#     form_class = UserCreationForm
#     model = User
#     template_name = 'registration/register.html'
#     success_url = reverse_lazy('persons:home')
#
#
# class LogoutView(RedirectView):
#     url = reverse_lazy('persons:login')
#
#     def get(self, request, *args, **kwargs):
#         logout(request)
#         return super(LogoutView, self).get(request, *args, **kwargs)
