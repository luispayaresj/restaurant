from django.db import models

from django.db.models.signals import post_save
from django.dispatch import receiver


class Client(models.Model):
    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    observations = models.CharField(max_length=50)

    def __str__(self):
        return f'{self.name} {self.last_name}'


class Waiter(models.Model):
    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    last_name_2 = models.CharField(max_length=50)

    def __str__(self):
        return f'{self.name} {self.last_name}'


