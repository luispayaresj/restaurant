from django import forms
from django.forms import TextInput

from persons.models import Client, Waiter


class ClientForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = ['name', 'last_name', 'observations']

        widgets = {
            'reference': TextInput(attrs={
                'class': "form-control mb-3",
                'style': 'max-width: 500px;',
            }),
        }


class WaiterForm(forms.ModelForm):
    class Meta:
        model = Waiter
        fields = ['name', 'last_name', 'last_name_2']
        widgets = {
            'reference': TextInput(attrs={
                'class': "form-control mb-3",
                'style': 'max-width: 500px;',
            }),
        }
