from django.contrib import admin

from persons.models import Client, Waiter


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'last_name', 'observations')
    search_fields = ('name', 'last_name')
    list_filter = ('name', 'last_name')


@admin.register(Waiter)
class WaiterAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'last_name', 'last_name_2')
    search_fields = ('name', 'last_name')
    list_filter = ('name', 'last_name')
