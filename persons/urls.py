from django.urls import path
from rest_framework import routers

from . import views
from .views import HomeView, ListClientView, ListWaiterView, RegisterClientView, RegisterWaiterView, UpdateClientView, \
    UpdateWaiterView, ClientDeleteView, WaiterDeleteView

app_name = 'persons'

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('persons/clients_list', ListClientView.as_view(), name='client_list'),
    path('persons/add_client', RegisterClientView.as_view(), name='add_client'),
    path('persons/update_client/<int:pk>/', (UpdateClientView.as_view()), name='update_client'),
    path('persons/delete_client/<int:pk>/', (ClientDeleteView.as_view()), name='delete_client'),
    path('persons/waiter_list', ListWaiterView.as_view(), name='waiter_list'),
    path('persons/add_waiter', RegisterWaiterView.as_view(), name='add_Waiter'),
    path('persons/update_waiter/<int:pk>/', (UpdateWaiterView.as_view()), name='update_waiter'),
    path('persons/delete_waiter/<int:pk>/', (WaiterDeleteView.as_view()), name='delete_waiter'),
    # path('register_user', RegisterView.as_view(), name='register_user'),
    # path('login/', LoginView.as_view(), name='login'),
    # path('logout/', LogoutView.as_view(), name='logout'),
    ]

router = routers.DefaultRouter()
router.register(r'client', views.ClientViewSet)
router.register(r'waiter', views.WaiterViewSet)