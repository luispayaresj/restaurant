from django import forms
from django.forms import TextInput

from tables.models import Table


class TableForm(forms.ModelForm):
    class Meta:
        model = Table
        fields = ['num_diners', 'location']

        widgets = {
            'reference': TextInput(attrs={
                'class': "form-control mb-3",
                'style': 'max-width: 500px;',
            }),
        }
