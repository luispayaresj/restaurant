from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from rest_framework import viewsets, permissions

from tables.forms import TableForm
from tables.models import Table
from tables.serializer import TableSerializer


class TableViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Table.objects.all()
    serializer_class = TableSerializer
    permission_classes = [permissions.IsAdminUser]


class ListTableView(ListView):
    model = Table
    queryset = Table.objects.all()
    template_name = 'tables/table_list.html'
    context_object_name = 'table_list'


class RegisterTableView(CreateView):
    form_class = TableForm
    model = Table
    template_name = 'tables/add_table.html'
    success_url = reverse_lazy('table:table_list')


class UpdateTableView(UpdateView):
    model = Table
    form_class = TableForm
    pk_url_kwarg = 'pk'
    template_name = 'tables/add_table.html'
    success_url = reverse_lazy('table:table_list')


class DeleteTableView(DeleteView):
    model = Table
    pk_url_kwarg = 'pk'
    template_name = 'tables/delete_table.html'
    success_url = reverse_lazy('table:table_list')
