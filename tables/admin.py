from django.contrib import admin

from tables.models import Table


@admin.register(Table)
class TableAdmin(admin.ModelAdmin):
    list_display = ('id', 'num_diners', 'location')
    search_fields = ('num_diners', 'location')
    list_filter = ('num_diners', 'location')
