from django.db import models


class Table(models.Model):
    num_diners = models.IntegerField()
    location = models.CharField(max_length=50)

    def __str__(self):
        return f'Table #{self.id}'
