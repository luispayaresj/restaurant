from django.urls import path
from rest_framework import routers

from . import views
from .views import ListTableView, RegisterTableView, UpdateTableView, DeleteTableView

app_name = 'table'

urlpatterns = [
    path('table_list', ListTableView.as_view(), name='table_list'),
    path('add_table', RegisterTableView.as_view(), name='add_Table'),
    path('update_table/<int:pk>/', UpdateTableView.as_view(), name='update_table'),
    path('delete_table/<int:pk>/', (DeleteTableView.as_view()), name='delete_table'),
    ]

router = routers.DefaultRouter()
router.register(r'tables', views.TableViewSet)